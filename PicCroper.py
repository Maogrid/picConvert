# -*- coding:utf-8 -*-
# Author: maoge
# DATE  :2020/12/14
# TIME  :下午11

import sys
import os
from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from UI.PicCroperUI import Ui_MainWindow
from PIL import Image


class PicCroper(QMainWindow,Ui_MainWindow):

    def __init__(self):
        super(PicCroper, self).__init__()
        self.setupUi(self)

        self.btn_crop.clicked.connect(self.cropImg)

    def cropImg(self):
        srcPath = self.lineEdit_scrImg.text()
        if not os.path.exists(srcPath):
            QMessageBox.warning(self, '注意', '原图路径不存在')
            return 0
        dirname = srcPath.split('/')[-1]
        outputname = dirname + '_croped'
        outputPath = os.path.join(os.path.dirname(srcPath), outputname)
        if not os.path.exists(outputPath):
            os.mkdir(outputPath)
        for orgImg in os.listdir(srcPath):
            if orgImg.split('.')[-1] in ['png','PNG','bmp','BMP']:
                eachImgPath = os.path.join(srcPath, orgImg)
                img = Image.open(eachImgPath)
                h, w = img.size
                size_min = min(h, w)
                if size_min > 300:
                    # croped
                    img_croped = img.crop((h//2-150, w//2-150,h//2+150, w//2+150))
                    savePath = os.path.join(outputPath,orgImg)
                    img_croped.save(savePath)
                else:
                    pass


def main():
    app = QApplication(sys.argv)
    Croper = PicCroper()
    Croper.show()
    Croper.raise_()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()